### Admin administration oauth
- `GET /admin/oauth2_provider`

![Data sources](img/admin_oauth2.png)

### Create client api key
- `GET /admin/oauth2_provider/application/add/`

![Data sources](img/create_oauth_client_api_key.png)

#### Params
- client id: (Automatic)
- user: (Mandatory) - admin user relation to api key
- redirect uris: (Not used)
- client type: (Mandatory) - Confidential
- authorization grant type: (Mandatory) - Resource owner password-based
- client secret: (Automatic)
- name: (optional) - api client key name
- skip authorization: (Not used)

#### Form example

![Data sources](img/example_oauth_client.png)

![Data sources](img/admin_oauth_admin.png)