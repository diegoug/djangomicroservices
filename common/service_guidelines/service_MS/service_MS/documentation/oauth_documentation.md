## Oauth

* [Oauth get token](#oauth-get-token)
* [Oauth refresh token](#oauth-refresh-token)
* [Test request using oauth](#test-request-using-oauth)

### Oauth get token
- `GET /o/token/`

#### Headers
- Content-Type: application/x-www-form-urlencoded

#### Body
- grant_type: (Mandatory) - password
- client_secret: (Mandatory) - oauth client secret id
- client_id: (Mandatory) - oauth client id
- username: (Mandatory) - Admin email
- password: (Mandatory) - Admin password

#### Success Response
```json
{
    "access_token": "vM7fNxJ5cJJUYuU1la0Ywv7y4GZFY4",
    "expires_in": 36000,
    "token_type": "Bearer",
    "scope": "read write",
    "refresh_token": "v2c5TAikjLgbx1r8XP3QlRTEcHMv0x"
}
```

#### Request python example
```python
import requests
url = 'host/o/token'

grant_type = 'password'
client_secret = 'XXXXXXXX'
client_id = 'XXXXXXXXXXXXXXXX'
username = 'admin@mesfix.com'
password = 'XXXXXXXXXXXX'
payload = 'grant_type={}&client_secret={}&client_id={}&username={}&password={}'.format(
    grant_type, client_secret, client_id, username, password
)
headers = {
    'content-type': "application/x-www-form-urlencoded",
    'cache-control': "no-cache",
}

response = requests.request("POST", url, data=payload, headers=headers)
```

#### Postman example

![Data sources](img/postman_get_token.png)

### Oauth refresh token
- `GET /o/token/`

#### Headers
- Content-Type: application/x-www-form-urlencoded

#### Body
- grant_type: (Mandatory) - refresh_token
- client_secret: (Mandatory) - oauth client secret id
- client_id: (Mandatory) - oauth client id
- refresh_token: (Mandatory) - Refresh token

#### Success Response
```json
{
    "access_token": "vM7fNxJ5cJJUYuU1la0Ywv7y4GZFY4",
    "expires_in": 36000,
    "token_type": "Bearer",
    "scope": "read write",
    "refresh_token": "v2c5TAikjLgbx1r8XP3QlRTEcHMv0x"
}
```

#### Postman example

![Data sources](img/postman_refresh_token.png)

### Test request using oauth
- `GET /oauth/verify/`
- `GET /oauth/verify/?help=true`

#### Headers
- Authorization: Bearer {access_token} (Mandatory) oauth access_token

#### Params
- help: (Optional) - Only the parameter without any value - Response HTML helper documentation.

#### Postman example 200

![Data sources](img/postman_oauth_verify.png)

#### Postman example 403

![Data sources](img/postman_oauth_verify_error.png)

#### Status Codes
- 200: ok
- 403: Forbidden
