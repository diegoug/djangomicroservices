import os

from .common_settings import *

SERVICE_NAME = os.environ.get('POSTGRES_SERVICE_MS_DATABASE_NAME', '')

if DEBUG:
    ALLOWED_HOSTS.append(
        '{}.localhost.com'.format(SERVICE_NAME).replace('_','-'))

ALLOWED_MICROSERVICES.append('service-ms-dev')

ALLOWED_HOSTS.extend(ALLOWED_MICROSERVICES)

# Application definition ------------------------------------------------------

INSTALLED_APPS.extend([
    'application',
])

MIDDLEWARE.insert(4, '{}.middleware.TimezoneMiddleware'.format(SERVICE_NAME))

ROOT_URLCONF = '{}.urls'.format(SERVICE_NAME)

# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash.
# Examples: "http://example.com/media/", "http://media.example.com/"
MEDIA_URL = '/{}/media/'.format(SERVICE_NAME)

WSGI_APPLICATION = '{}.wsgi.application'.format(SERVICE_NAME)

# Database routers ------------------------------------------------------------

DATABASE_ROUTERS = [
    '{}.django_admin_db_router.DjangoRouter'.format(SERVICE_NAME),
    '{0}.{0}_router.MicroServiceRouter'.format(SERVICE_NAME)
]

# Static files (CSS, JavaScript, Images) --------------------------------------
# https://docs.djangoproject.com/en/2.2/howto/static-files/

STATIC_URL = '/static/'

# external credentials --------------------------------------------------------
