const DEMO_MS_HOST = process.env.DEMO_MS_HOST || "";
const DEMO_MS_PORT = process.env.DEMO_MS_PORT || "";
exports.DEMO_MS = `http://${DEMO_MS_HOST}:${DEMO_MS_PORT}`;
