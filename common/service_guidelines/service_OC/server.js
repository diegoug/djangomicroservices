// requirements ---------------------------------------------------------------
const koa = require('koa');
const bodyParser = require('koa-body');
// ----------------------------------------------------------------------------
// koa server -----------------------------------------------------------------
// ----------------------------------------------------------------------------
const server = new koa();
server.use(bodyParser({
    formidable: {uploadDir: './uploads'},
    multipart: true,
    urlencoded: true
}));
// routes
const routerDemo = require("./router/demo");
server.use(routerDemo.routes());
// run server
server.listen(process.env.SERVICE_OC_PORT || 0, () => {
    console.log(`Server listening on port: ${process.env.SERVICE_OC_PORT || 0}`);
});