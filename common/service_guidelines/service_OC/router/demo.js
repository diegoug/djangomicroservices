const DOCKER_PROD = process.env.DOCKER_PROD || false;

const Router = require('koa-router');
const router = new Router();

const axios = require("axios");
axios.interceptors.response.use(null, error => {
    return error.response;
});

const hosts = require("../helpers/hosts.js");
const DEMO_MS = hosts.DEMO_MS;

router.get("demo/api/v1/", async (ctx) => {
    // request profile
    let requestUrl = `${DEMO_MS}/`;
    let response = await axios.get(requestUrl);
    if (response.status !== 200){
        ctx.status = response.status;
        ctx.body = response.data;
        return
    }
    ctx.status = 200;
    ctx.body = {
        status: 'success',
        message: 'hello, world!'
    };
});

module.exports = router;