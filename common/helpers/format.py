import decimal
import datetime
import uuid as uuid_lib


def dict_format(obj):
    for key in obj:
        if isinstance(obj[key], decimal.Decimal):
            obj[key] = float(obj[key])
        if isinstance(obj[key], uuid_lib.UUID):
            obj[key] = str(obj[key])
        if isinstance(obj[key], datetime.datetime):
            obj[key] = obj[key].strftime("%Y-%m-%d %H-%M-%S.%f")
        if isinstance(obj[key], datetime.timedelta):
            obj[key] = str(obj[key])
    return obj

def tuple_to_dict(obj, field=None):
    if field and field == 'value':
        return list(value for key, value in obj)
    elif field and field == 'key':
        return list(key for key, value in obj)
    return dict((value, key) for key, value in obj)
