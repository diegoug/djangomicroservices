
from django.http import HttpResponseForbidden


class ModelExtended:
    def get_str_unicode(self):
        if hasattr(self, 'name'):
            return self.name
        elif hasattr(self, 'arguments'):
            return u"{} {}".format(self.get_function_name(), self.arguments)
        else:
            return u"Id: {}".format(self.id)


class ViewExtended:
    @staticmethod
    def validate_get_int(s):
        if s is False:
            return False, False
        try:
            int(s)
            return True, int(s)
        except ValueError:
            pass
        try:
            import unicodedata
            unicodedata.numeric(s)
            return True, unicodedata.numeric(s)
        except (TypeError, ValueError):
            pass
        return False, False


class ExtendedDispatch:
    def dispatch(self, request, *args, **kwargs):
        # validate oauth
        if request.method.upper() == "OPTIONS":
            return super().dispatch(request, *args, **kwargs)

        verify_request = self.verify_request(request)
        if verify_request[0] and request.user:
            request.resource_owner = request.user
            if request.user.is_anonymous and hasattr(
                verify_request[1], 'user'):
                request.user = verify_request[1].user
            self.user = request.user
        else:
            return HttpResponseForbidden(
                '403 App OAuth/Forbidden', content_type='application/json')
        # handle requests
        method = 'http_method' if hasattr(request, 'http_method') else 'method'
        request_method = getattr(request, method).lower()
        handler = self.http_method_not_allowed
        if request_method in self.http_method_names:
            handler = getattr(
                self, request_method, self.http_method_not_allowed)
            if getattr(request, method).upper() == 'GET' and not kwargs.get(
                    self.restful_query_param, False):
                self.custom_action = 'list'
            if self.custom_action:
                handler = getattr(
                    self, '{}_{}'.format(request_method, self.custom_action),
                    handler if handler else self.http_method_not_allowed)
        return handler(request, *args, **kwargs)
