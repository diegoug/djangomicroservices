
import re
import requests
import tempfile

from django.core import files


def camelcase_to_snake_case(name):
    s1 = re.sub('(.)([A-Z][a-z]+)', r'\1_\2', name)
    return re.sub('([a-z0-9])([A-Z])', r'\1_\2', s1).lower()


def get_file(url, headers):
    # Steam the image from the url
    request = requests.get(url, stream=True, headers=headers)

    # Was the request OK?
    if request.status_code != requests.codes.ok:
        # Nope, error handling, skip file etc etc etc
        return None, None

    # Get the filename from the url, used for saving later
    file_name = url.split('/')[-1]

    # Create a temporary file
    tmp_file = tempfile.NamedTemporaryFile()

    # Read the streamed image in sections
    for block in request.iter_content(1024 * 8):

        # If no more file then stop
        if not block:
            break

        # Write image block to temporary file
        tmp_file.write(block)

    return file_name, files.File(tmp_file)
