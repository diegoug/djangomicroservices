
# Visual Studio Code

## Configure Visual Studio Code to work with Docker

Add repository folder to your Visual Studio Code project

### Configure hosts file

This is for the computer to find a specific host or service using a hostname

#### Add docker host to hosts file

Add this line

```file
127.0.0.1 dockerhost
```

#### Add service-a-ms service-b-ms to host file

Add this line

```file
127.0.0.1   backend-for-frontend-ms.localhost.com
127.0.0.1   backend-for-frontend-oc.localhost.com
127.0.0.1   user-ms.localhost.com
127.0.0.1   service-a-ms.localhost.com
127.0.0.1   service-b-ms.localhost.com
```

##### Linux path to host file

/etc/hosts

##### windows path to host file

C:\Windos\System32\drivers\etc\hosts

### Configure Visual Studio Code

#### Running and Debuging containers with Remote Explorer

The plugin added called Remote Explorer allows to see what containers exist in the system and which are running (by a simple round mark in the screen). The icon can be found on the left side of the screen that looks like a remote conection icon.

You must run the containers with the instructions given in the [README] in the section "Commands to work with docker".

To access any of these containers in a way that migth look they are in local we right click on "Attach Container" on the running container, or, we place the cursor on top of the name and an icon should show on the right side that looks like a window icon that is also "Attach Container" and from there a new window of Visual Studio Code should open and from there we could see the code as if was local. Next is shown and example:

![Enter a Container as local](img/remote_explorer.png)

**NOTE:** If is the first time running the container the route of the project in this new Visual Studio Code window should be defined in the explorer of file to the route: "/opt/app" and from there you should be able to see the files.

You must run the project being inside the container and no error should be given. To run the project can be done with Ctrl + Shift + D or going to the left side of Visual Studio Code window and find the Run button that looks like a play and a bug symbol and there also in the left side but on top, there should be a green play button that must be clicked and should run the project.

To debug the project must be running and in the line of code where you want to debug in the left side of the window where the code is shown click on the left side where the line number appears to make a red ball remain permanent, and there the code will stop if it pass by when is being used the project. Next an example of the to run the project and how is seen the red ball in the code lines:

![Runningn Server and Debuggin point](img/run.png)
