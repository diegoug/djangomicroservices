from django.apps import AppConfig


class SchedulerConfig(AppConfig):
    name = 'data_model'
    verbose_name = 'data_models'
