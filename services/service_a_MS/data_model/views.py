import json

from math import ceil
from oauth2_provider.views.generic import ProtectedResourceView

from django.views.decorators.csrf import csrf_exempt
from django.views.generic import View
from django.utils.decorators import method_decorator
from django.http import HttpResponse, HttpResponseForbidden
from django.core.exceptions import ValidationError
from django.contrib.auth.decorators import login_required, permission_required
from django.db.models.fields.related import ManyToManyField

from .models import DataModel

from common.helpers.format import dict_format

class ApiEndpoint(ProtectedResourceView):
    def get(self, request, *args, **kwargs):
        return HttpResponse('Hello, OAuth2!')

@method_decorator(csrf_exempt, name='dispatch')
class DataModelView(ProtectedResourceView, View):
    custom_action = None
    user = None

    def dispatch(self, request, *args, **kwargs):
        # validate oauth
        if request.method.upper() == "OPTIONS":
            return super().dispatch(request, *args, **kwargs)

        valid, r = self.verify_request(request)
        if valid:
            request.resource_owner = r.user
            self.user = r.user
        else:
            return HttpResponseForbidden()
        # handle requests
        request_method = request.method.lower()
        handler = self.http_method_not_allowed
        if request_method in self.http_method_names:
            handler = getattr(
                self, request_method, self.http_method_not_allowed)
            if request.method == 'GET' and not kwargs.get('uid', False):
                self.custom_action = 'list'
            if self.custom_action:
                handler = getattr(
                    self, '{}_{}'.format(request_method, self.custom_action),
                    handler if handler else self.http_method_not_allowed)
        return handler(request, *args, **kwargs)

    @permission_required('data_model.view_datamodel', raise_exception=True)
    def get(self, request, *args, **kwargs):
        uid = kwargs.get('uid')
        data_model = DataModel.objects.filter(uid=uid)
        if not data_model.exists():
            return HttpResponse(status=404)

        data_model = dict_format(data_model.values().first())
        return HttpResponse(json.dumps(data_model),
                            content_type="application/json", status=200)

    @permission_required('data_model.delete_datamodel', raise_exception=True)
    def delete(self, request, *args, **kwargs):
        uid = kwargs.get('uid')
        data_model = DataModel.objects.filter(uid=uid)
        if not data_model.exists():
            return HttpResponse(status=404)
        data_model.delete()

        return HttpResponse(status=202)

    @permission_required('data_model.view_datamodel', raise_exception=True)
    def get_list(self, request, *args, **kwargs):

        valid_page, page = self.validate_get_int(
            request.GET.get('page', False))
        if not valid_page or page < 1:
            page = 1

        valid_limit, limit = self.validate_get_int(
            request.GET.get('limit', False))
        if not valid_limit:
            limit = 10
        elif limit > 50:
            limit = 50
        elif limit < 1:
            limit = 1

        data_models = DataModel.objects.all().values()

        count = data_models.count()
        pages = ceil(count / limit)
        offset = (page - 1) * limit

        data_models = data_models[offset:limit]
        data_models = [dict_format(data_model) for data_model in data_models]

        resp = {
            'data': data_models,
            'page': page,
            'limit': limit,
            'count': count,
            'pages': pages,
            'message': 'Data model retrieved successfully.'
        }

        return HttpResponse(json.dumps(resp),
                            content_type="application/json", status=200)

    @permission_required('data_model.add_datamodel', raise_exception=True)
    def post(self, request, *args, **kwargs):
        invalid_request = False
        if request.content_type != 'application/json':
            invalid_request = True
        if not request.body:
            invalid_request = True
        if invalid_request:
            message = 'you can not create the data model, ' \
                      'because is invalid content type ' \
                      'or there are no parameters'
            return HttpResponse(message, status=403)

        body = request.body
        json_data = json.loads(body)
        try:
            data_model = DataModel(**json_data)
            data_model.full_clean()
        except TypeError as e:
            message = str(e).replace('DataModel() ', '')
            return HttpResponse(message, status=403)
        except ValidationError as e:
            return HttpResponse(str(e),
                                content_type="application/json", status=403)
        data_model.save()

        data_model = dict_format(DataModel.objects.filter(
            id=data_model.id).values().first())
        return HttpResponse(json.dumps(data_model), status=201,
                            content_type="application/json")

    @permission_required('data_model.change_datamodel', raise_exception=True)
    def put(self, request, *args, **kwargs):
        invalid_request = False
        if request.content_type != 'application/json':
            invalid_request = True
        if not request.body:
            invalid_request = True
        if invalid_request:
            message = 'you can not create the data model, ' \
                      'because is invalid content type ' \
                      'or there are no parameters'
            return HttpResponse(message, status=403)

        body = request.body
        json_data = json.loads(body)

        if 'uid' not in kwargs:
            message = 'you can not update data model, ' \
                      'because uid parameter are missing.'
            return HttpResponse(message, status=403)
        uid = kwargs.get('uid')

        data_model = DataModel.objects.filter(uid=uid)
        if not data_model.exists():
            message = 'you can not update the data model, ' \
                      'because It was not found a control area with this uuid.'
            return HttpResponse(message, status=404)

        # extract m2m data to update
        m2m_keys = []
        for attr, value in DataModel.__dict__.items():
            if hasattr(value, 'field'):
                if isinstance(value.field, ManyToManyField):
                    m2m_keys.append(attr)
        m2m_dict = {k: json_data[k] for k in m2m_keys}
        for key in m2m_keys:
            if key in json_data:
                del json_data[key]
        # validate updated data
        data_model_dict = dict_format(data_model.first().values())
        json_data = data_model_dict.update(json_data)
        try:
            data_model = DataModel(**json_data)
            data_model.full_clean()
        except TypeError as e:
            message = str(e).replace('DataModel() ', '')
            return HttpResponse(message, status=403)
        except ValidationError as e:
            return HttpResponse(str(e),
                                content_type="application/json", status=403)
        # update data
        data_model.update(**json_data)
        # update m2m relations
        if m2m_keys:
            data_model_m2m = data_model.first()
            for key in m2m_keys:
                m2m_relation = getattr(data_model_m2m, key)
                m2m_relation.clear()
                for data_set in m2m_dict[key]:
                    m2m_relation.set(data_set)
        # response data model
        data_model = dict_format(data_model.first().values())
        return HttpResponse(json.dumps(data_model),
                            content_type="application/json", status=201)

    @staticmethod
    def validate_get_int(s):
        if s is False:
            return False, False
        try:
            int(s)
            return True, int(s)
        except ValueError:
            pass
        try:
            import unicodedata
            unicodedata.numeric(s)
            return True, unicodedata.numeric(s)
        except (TypeError, ValueError):
            pass
        return False, False