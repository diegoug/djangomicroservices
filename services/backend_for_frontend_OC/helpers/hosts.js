const USER_MS_HOST = process.env.USER_MS_HOST || "";
const USER_MS_PORT = process.env.USER_MS_PORT || "";
exports.USER_MS = `http://${USER_MS_HOST}:${USER_MS_PORT}`;

const SERVICE_A_MS_HOST = process.env.SERVICE_A_MS_HOST || "";
const SERVICE_A_MS_PORT = process.env.SERVICE_A_MS_PORT || "";
exports.SERVICE_A_MS = `http://${SERVICE_A_MS_HOST}:${SERVICE_A_MS_PORT}`;

const SERVICE_B_MS_HOST = process.env.SERVICE_B_MS_HOST || "";
const SERVICE_B_MS_PORT = process.env.SERVICE_B_MS_PORT || "";
exports.SERVICE_B_MS = `http://${SERVICE_B_MS_HOST}:${SERVICE_B_MS_PORT}`;