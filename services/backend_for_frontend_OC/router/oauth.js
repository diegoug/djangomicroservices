const Router = require('koa-router');
const router = new Router();

const axios = require("axios");
axios.interceptors.response.use(null, error => {
    return error.response;
});

const hosts = require("../helpers/hosts.js");
const USER_MS = hosts.USER_MS;

router.get('/oauth/aplication/', async (ctx) => {
    let requestUrl = `${USER_MS}/oauth/aplication/`;
    let response = await axios({
        method: 'get',
        url: requestUrl,
        headers: { 
            'cookie': ctx.req.headers.cookie
        },
    });
    ctx.status = response.status;
    ctx.body = response.data;
});

router.post('/oauth/active_token/', async (ctx) => {
    let requestUrl = `${USER_MS}/oauth/active_token/`;
    let response = await axios({
        method: 'post',
        url: requestUrl,
        headers: { 
            'cookie': ctx.req.headers.cookie
        },
        data: ctx.request.body
    });
    ctx.status = response.status;
    ctx.body = response.data;
});

router.post('/oauth/token/', async (ctx) => {
    let requestUrl = `${USER_MS}/oauth/token/`;
    let response = await axios({
        method: 'post',
        url: requestUrl,
        headers: { 
            'cookie': ctx.req.headers.cookie,
            'content-type': ctx.req.headers['content-type'],
            'cache-control': ctx.req.headers['cache-control']
        },
        data: `grant_type=${ctx.request.body['grant_type']}&client_secret=${ctx.request.body['client_secret']}&client_id=${ctx.request.body['client_id']}`
    });
    ctx.status = response.status;
    ctx.body = response.data;
});

module.exports = router;