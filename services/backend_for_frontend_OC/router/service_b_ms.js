const DOCKER_PROD = process.env.DOCKER_PROD || false;

const Router = require('koa-router');
const router = new Router();

const axios = require("axios");

const hosts = require("../helpers/hosts.js");
const SERVICE_B_MS = hosts.SERVICE_B_MS;

router.get("/data/api/v1/obj/", async (ctx) => {
    let requestUrl = `${SERVICE_B_MS}/data/api/v1/obj/`;
    let response = await axios({
        method: 'GET',
        url: requestUrl,
        headers: { 
            'cookie': ctx.req.headers.cookie,
            'authorization': ctx.req.headers['authorization']
        },
        params: ctx.request.query
    });
    ctx.status = response.status;
    ctx.body = response.data;
});

router.get('/data/api/v1/obj/:uid/', async (ctx) => {
    let requestUrl = `${SERVICE_B_MS}/data/api/v1/obj/${ctx.params['uid']}/`;
    let response = await axios({
        method: 'GET',
        url: requestUrl,
        headers: { 
            'cookie': ctx.req.headers.cookie,
            'authorization': ctx.req.headers['authorization']
        },
        params: ctx.request.query
    });
    ctx.status = response.status;
    ctx.body = response.data;
});

router.post('/data/api/v1/obj/', async (ctx) => {
    let requestUrl = `${SERVICE_B_MS}/data/api/v1/obj/`;
    let response = await axios({
        method: 'POST',
        url: requestUrl,
        headers: { 
            'content-type': 'application/json',
            'cookie': ctx.req.headers.cookie,
            'authorization': ctx.req.headers['authorization']
        },       
        data:ctx.request.body
    });
    ctx.status = response.status;
    ctx.body = response.data;
});

router.put('/data/api/v1/obj/:uid/', async (ctx) => {
    let requestUrl = `${SERVICE_B_MS}/data/api/v1/obj/${ctx.params['uid']}/`;
    delete ctx.request.body.accion;
    let response = await axios({
        method: 'PUT',
        url: requestUrl,
        headers: { 
            'content-type': 'application/json',
            'cookie': ctx.req.headers.cookie,
            'authorization': ctx.req.headers['authorization']
        },       
        data:ctx.request.body
    });
    ctx.status = response.status;
    ctx.body = response.data;
});

router.delete('/data/api/v1/obj/:uid/', async (ctx) => {
    let requestUrl = `${SERVICE_B_MS}/data/api/v1/obj/${ctx.params['uid']}/`;
    let response = await axios({
        method: 'DELETE',
        url: requestUrl,
        headers: { 
            'cookie': ctx.req.headers.cookie,
            'authorization': ctx.req.headers['authorization']
        },
        params: ctx.request.query
    });
    ctx.status = response.status;
    ctx.body = response.data;
});

module.exports = router;