const DOCKER_PROD = process.env.DOCKER_PROD || false;

const Router = require('koa-router');
const router = new Router();

const axios = require("axios");

const hosts = require("../helpers/hosts.js");
const USER_MS = hosts.USER_MS;

router.get('/user/api/v1/user/', async (ctx) => {
    let requestUrl = `${USER_MS}/user/api/v1/user/`;
    let response = await axios({
        method: 'GET',
        url: requestUrl,
        headers: { 
            'cookie': ctx.req.headers.cookie,
            'authorization': ctx.req.headers['authorization']
        },
        params: ctx.request.query
    });
    ctx.status = response.status;
    ctx.body = response.data;
});

router.get('/user/api/v1/user/:email/', async (ctx) => {
    let requestUrl = `${USER_MS}/user/api/v1/user/${ctx.params['email']}/`;
    let response = await axios({
        method: 'GET',
        url: requestUrl,
        headers: { 
            'cookie': ctx.req.headers.cookie,
            'authorization': ctx.req.headers['authorization']
        },
        params: ctx.request.query
    });
    ctx.status = response.status;
    ctx.body = response.data;
});

router.get('/user/api/v1/user/:id/', async (ctx) => {
    let requestUrl = `${USER_MS}/user/api/v1/user/${ctx.params['id']}/`;
    let response = await axios({
        method: 'GET',
        url: requestUrl,
        headers: { 
            'cookie': ctx.req.headers.cookie,
            'authorization': ctx.req.headers['authorization']
        },
        params: ctx.request.query
    });
    ctx.status = response.status;
    ctx.body = response.data;
});

router.post('/user/api/v1/user/', async (ctx) => {
    let requestUrl = `${USER_MS}/user/api/v1/user/`;
    let response = await axios({
        method: 'POST',
        url: requestUrl,
        headers: { 
            'content-type': 'application/json',
            'cookie': ctx.req.headers.cookie,
            'authorization': ctx.req.headers['authorization']
        },       
        data:ctx.request.body
    });
    ctx.status = response.status;
    ctx.body = response.data;
});

router.put('/user/api/v1/user/:email', async (ctx) => {
    delete ctx.request.body.csrfmiddlewaretoken;
    let requestUrl = `${USER_MS}/user/api/v1/user/${ctx.params['email']}/`;
    let response = await axios({
        method: 'PUT',
        url: requestUrl,
        headers: { 
            'content-type': 'application/json',
            'cookie': ctx.req.headers.cookie,
            'authorization': ctx.req.headers['authorization']
        },       
        data:ctx.request.body
    });
    ctx.status = response.status;
    ctx.body = response.data;
});

router.delete('/user/api/v1/user/:email', async (ctx) => {
    let requestUrl = `${USER_MS}/user/api/v1/user/${ctx.params['email']}/`;
    let response = await axios({
        method: 'DELETE',
        url: requestUrl,
        headers: { 
            'cookie': ctx.req.headers.cookie,
            'authorization': ctx.req.headers['authorization']
        },
        params: ctx.request.query
    });
    ctx.status = response.status;
    ctx.body = response.data;
});