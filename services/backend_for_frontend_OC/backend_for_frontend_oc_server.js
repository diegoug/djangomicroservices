// requirements ---------------------------------------------------------------
var koa = require('koa');
var bodyParser = require('koa-body');
// import routes --------------------------------------------------------------
const routerMinTrans = require("./router/service_a_ms");
// config ---------------------------------------------------------------------
const external_webservice_oc_server = new koa();
external_webservice_oc_server.use(bodyParser({
    formidable: {uploadDir: './uploads'},
    multipart: true,
    urlencoded: true
}));
// routes ---------------------------------------------------------------------
external_webservice_oc_server.use(routerMinTrans.routes());
// run server -----------------------------------------------------------------
external_webservice_oc_server.listen(process.env.BACKEND_FOR_FRONTEND_OC_HOST || "0.0.0.0", process.env.BACKEND_FOR_FRONTEND_OC_PORT || 0, () => {
    console.log(`Server listening on hots: ${process.env.BACKEND_FOR_FRONTEND_OC_HOST || "0.0.0.0"}, port: ${process.env.BACKEND_FOR_FRONTEND_OC_PORT || 0}`);
});