
from django.db import models
from django.utils import timezone
from django.utils.translation import ugettext as _

class DataModel(models.Model):
    uid = models.IntegerField(
        blank=True, null=True)
    name = models.CharField(
        _("name"), max_length=500)
    active = models.BooleanField(
        _("active"), default=True)
    created = models.DateTimeField(
        _('Created'), auto_now_add=True)
    modified = models.DateTimeField(
        _('Modified'), auto_now=True)
    cell_phone = models.IntegerField(
        _("cell_phone"), blank=True, null=True)

    class Meta:
        verbose_name = _("data model")
        verbose_name_plural = _("data models")

    def __str__(self):
        return self.get_str_unicode()

    def __unicode__(self):
        return self.get_str_unicode()

    def get_str_unicode(self):
        if self.name:
            return self.name
        elif self.arguments:
            return u"{} {}".format(self.get_function_name(), self.arguments)
        else:
            return u"Task {}".format(self.id)

    def save(self):
        uid = 1000
        if not self.uid and DataModel.objects.all().exists():
            uid = DataModel.objects.order_by('uid').last().uid + 1
        if not self.uid:
            self.uid = uid
        super(DataModel, self).save()
