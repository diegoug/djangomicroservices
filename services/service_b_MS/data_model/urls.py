
from django.conf.urls import url

from .views import DataModelView, ApiEndpoint

urlpatterns = [
    url(r'^api/v1/data_model/$',
        DataModelView.as_view(),
        name='data_model_list'),

    url(r'^api/v1/data_model/(?P<uid>[-\w]+)/$',
        DataModelView.as_view(),
        name='data_model_get'),
]
