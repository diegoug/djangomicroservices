from django.contrib import admin

from .models import DataModel


@admin.register(DataModel)
class DataModelAdmin(admin.ModelAdmin):
    pass
