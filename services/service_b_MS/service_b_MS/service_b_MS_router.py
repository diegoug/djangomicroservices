from django.conf import settings

class MicroServiceRouter:
    """
    A router to control all database operations on models in the application.
    """
    django_db_labels = {'admin', 'auth', 'profiles', 'sessions',
                        'oauth2_provider', 'contenttypes'}

    def db_for_read(self, model, **hints):
        """
        Reads go to a SERVICE_NAME database.
        """
        return settings.SERVICE_NAME

    def db_for_write(self, model, **hints):
        """
        Writes always go to SERVICE_NAME database.
        """
        return settings.SERVICE_NAME

    def allow_relation(self, obj1, obj2, **hints):
        """
        Relations between objects are allowed if both objects are
        in the SERVICE_NAME database.
        """
        if obj1._state.db in settings.SERVICE_NAME and \
                obj2._state.db in settings.SERVICE_NAME:
            return True
        return None

    def allow_migrate(self, db, app_label, model_name=None, **hints):
        """
        Make sure the auth and contenttypes apps only appear in the
        'auth_db' database.
        """
        if app_label in self.django_db_labels:
            return None
        return db == settings.SERVICE_NAME
