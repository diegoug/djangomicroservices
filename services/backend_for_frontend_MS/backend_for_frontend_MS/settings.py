import os

from .common_settings import *

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = os.environ.get('BACKEND_FOR_FRONTEND_MS_DJANGO_KEY', '')

SERVICE_NAME = os.environ.get('BACKEND_FOR_FRONTEND_MS_NAME', '')

if DEBUG:
    ALLOWED_HOSTS.append(
        '{}.localhost.com'.format(SERVICE_NAME).replace('_','-'))
else: 
    DOMAIN_NAME = os.environ.get('DOMAIN_NAME', '')
    ALLOWED_HOSTS.append('{}'.format(DOMAIN_NAME))
    

# Application definition ------------------------------------------------------

INSTALLED_APPS.extend([
    'backend_for_frontend_MS',
    'user_MS',
])

MIDDLEWARE.insert(
    4, '{}.middleware.MultipleProxyMiddleware'.format(SERVICE_NAME))
MIDDLEWARE.insert(
    5, '{}.middleware.TimezoneMiddleware'.format(SERVICE_NAME))
MIDDLEWARE.insert(
    11, '{}.middleware.SetOautClientCredentials'.format(SERVICE_NAME))
MIDDLEWARE.insert(
    12, '{}.middleware.CompanyHostMiddleware'.format(SERVICE_NAME))

ROOT_URLCONF = '{}.urls'.format(SERVICE_NAME)

# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash.
# Examples: "http://example.com/media/", "http://media.example.com/"
MEDIA_URL = '/{}/media/'.format(SERVICE_NAME)


WSGI_APPLICATION = '{}.wsgi.application'.format(SERVICE_NAME)

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'OPTIONS': {
            'context_processors': [
                'django.contrib.auth.context_processors.auth',
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.template.context_processors.i18n',
                'django.template.context_processors.media',
                'django.template.context_processors.static',
                'django.template.context_processors.tz',
                'django.contrib.messages.context_processors.messages'
            ],
            'loaders': [
                ('django.template.loaders.filesystem.Loader',
                 [os.path.join(BASE_DIR, 'templates')]),
                'django.template.loaders.app_directories.Loader'
            ]
        },
    },
]

STATICFILES_FINDERS = [
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
]

# Database --------------------------------------------------------------------
DATABASE_ROUTERS = [
    '{}.django_admin_db_router.DjangoRouter'.format(SERVICE_NAME)
]

# Static files (CSS, JavaScript, Images) --------------------------------------
# https://docs.djangoproject.com/en/2.2/howto/static-files/

STATIC_URL = '/static/'



# backend for frontend --------------------------------------------------------
LOGIN_REDIRECT_URL = '/user-ms/profile/'

# external --------------------------------------------------------------------
BACKEND_FOR_FRONTEND_OC_HOST = os.environ.get(
    'BACKEND_FOR_FRONTEND_OC_HOST', '')
BACKEND_FOR_FRONTEND_OC_PORT = os.environ.get(
    'BACKEND_FOR_FRONTEND_OC_PORT', '')
BACKEND_FOR_FRONTEND_OC = 'http://{}:{}'.format(
    BACKEND_FOR_FRONTEND_OC_HOST, BACKEND_FOR_FRONTEND_OC_PORT)
