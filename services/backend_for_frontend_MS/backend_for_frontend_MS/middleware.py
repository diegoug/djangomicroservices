import pytz
import json
import requests

from datetime import datetime

from django.utils import timezone
from django.http import Http404
from django.utils.deprecation import MiddlewareMixin
from django.conf import settings

from oauth2_provider.models import Application, AccessToken


class MultipleProxyMiddleware:
    FORWARDED_FOR_FIELDS = [
        'HTTP_X_FORWARDED_FOR',
        'HTTP_X_FORWARDED_HOST',
        'HTTP_X_FORWARDED_SERVER',
    ]

    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        """
        Rewrites the proxy headers so that only the most
        recent proxy is used.
        """
        for field in self.FORWARDED_FOR_FIELDS:
            if field in request.META:
                if ',' in request.META[field]:
                    parts = request.META[field].split(',')
                    request.META[field] = parts[-1].strip()
        return self.get_response(request)


class TimezoneMiddleware(MiddlewareMixin):
    def process_request(self, request):
        timezone.activate(pytz.timezone('America/Bogota'))


class CompanyHostMiddleware(MiddlewareMixin):
    def process_request(self, request):
        request.user.branding = 'custom'
        return


class SetOautClientCredentials(MiddlewareMixin):
    def process_request(self, request):
        
        if request.user.is_anonymous:
            return
        
        cookies = {
            'sessionid': request.session.session_key
        }

        url = '{}/oauth/aplication/'.format(settings.BACKEND_FOR_FRONTEND_OC)

        oauth_application = requests.get(url=url, cookies=cookies)

        if oauth_application.status_code != 200:
                raise Http404
        oauth_application = json.loads(oauth_application.text)

        url = '{}/oauth/active_token/'.format(settings.BACKEND_FOR_FRONTEND_OC)
        request_token = requests.post(url=url, cookies=cookies, 
                                     data=oauth_application)
        
        if request_token.status_code != 200:
                raise Http404
        request_token = json.loads(request_token.text)

        if not request_token['access_token']:

            url = '{}/oauth/token/'.format(settings.BACKEND_FOR_FRONTEND_OC)
            
            data = {
                'grant_type': 'client_credentials',
                'client_secret': oauth_application['client_secret'],
                'client_id': oauth_application['client_id']
            }
            
            headers = {
                'content-type': "application/x-www-form-urlencoded",
                'cache-control': "no-cache",
            }
            
            request_token = requests.post(url=url, cookies=cookies, 
                data=data, headers=headers)

            if request_token.status_code != 200:
                raise Http404
            request_token = json.loads(request_token.text)
        
        request.user.oauth_access_token = request_token['access_token']
        
        return
