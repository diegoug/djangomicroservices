
from django.views.generic import View

import markdown2
from oauth2_provider.views.generic import ProtectedResourceView

from django.http import JsonResponse, HttpResponse
from django.middleware.csrf import get_token


class ApiVerify(ProtectedResourceView, View):
    def get(self, request, *args, **kwargs):

        user = request.resource_owner
        response = {
            'user': {
                'email': user.email,
            },
            'csrf_token': get_token(request),
        }
        try:
            response['user']['username'] = user.username
        except AttributeError:
            pass
        return JsonResponse(response)


class HealthCheck(View):

    def get(self, request, *args, **kwargs):
        # ToDo: check if database engine is working
        return HttpResponse(status=204)
