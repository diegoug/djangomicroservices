var cell = false;
var tablet = false;
var desktop = false;

var onsearch = false;
var hamMenuFloat = false;
var hamMenuPush = false;

window.onresize = function () {
    // celular
    if (window.matchMedia("(min-width: 320px)").matches && window.matchMedia("(max-width: 600px)").matches) {
        console.log('cel');

        if (hamMenuPush == true) {
            offHamMenuPush();
        }

        cell = true;
        tablet = false;
        desktop = false;
    }
    // tablet
    if (window.matchMedia("(min-width: 601px)").matches && window.matchMedia("(max-width: 1040px)").matches) {
        console.log('tablet');

        if (hamMenuFloat == true) {
            offHamMenuFloat();
        }

        if (hamMenuPush == true && desktop == true) {
            offHamMenuPush();
        }

        cell = false;
        tablet = true;
        desktop = false;
    }
    // desktop
    if (window.matchMedia("(min-width: 1041px)").matches) {
        console.log('desk');

        if (hamMenuPush == false) {
            onHamMenuPush();
        }

        cell = false;
        tablet = false;
        desktop = true;
    }
};

function onHamMenuFloat() {
    if (hamMenuFloat == false && cell == true) {
        document.getElementById("userTwo").style.width = "250px";
        document.getElementById("menu").style.width = "250px";
        hamMenuFloat = true;
    }
}

function offHamMenuFloat() {
    if (hamMenuFloat == true && cell == true) {
        document.getElementById("userTwo").style.width = "0px";
        document.getElementById("menu").style.width = "0px"
        hamMenuFloat = false;
    }
}

function onHamMenuPush() {
    if (hamMenuPush == false && tablet == true) {
        document.getElementById("conten").style.marginLeft = "250px";
        document.getElementById("userTwo").style.width = "250px";
        document.getElementById("menu").style.width = "250px";
        hamMenuPush = true;
    }
}

function offHamMenuPush() {
    if (hamMenuPush == true && (tablet == true || desktop == true)) {
        document.getElementById("conten").style.marginLeft = "0px";
        document.getElementById("userTwo").style.width = "0px";
        document.getElementById("menu").style.width = "0px";
        hamMenuPush = false;
    }
}

function revisaPagina(){
    var nombre = location.pathname;
    nombre = nombre.split('/');
    var report = document.getElementById("reportli");
    var workorder = document.getElementById("workorderli");
    var equipment = document.getElementById("equipmentli");
    var users = document.getElementById("usersli");
    if(nombre[2] == "user"||nombre[2] == "users"){
        users.style.backgroundColor = "#3f5cb4";
    }else if(nombre[2] =="report"||nombre[2] == "reports"){
        report.style.backgroundColor = "#3f5cb4";
    }else if(nombre[2] =="workorder"||nombre[2] == "workorders"){
        workorder.style.backgroundColor = "#3f5cb4";
    }else if(nombre[2] =="equipment"||nombre[2] == "equipment"){
        equipment.style.backgroundColor = "#3f5cb4";
    }
}

