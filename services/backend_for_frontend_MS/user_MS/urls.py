
from django.urls import include, re_path

from .views import UserTemplateView 


urlpatterns = [
    re_path(
        'users/', 
        UserTemplateView.as_view(), 
        name='users'),
    re_path(
        r'^user/(?P<email>[\w.@+-]+)/$', 
        UserTemplateView.as_view(), 
        name='user')
]