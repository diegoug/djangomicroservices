import requests
import json
from datetime import datetime

from django.conf import settings
from django.urls import reverse
from django.http import HttpResponseRedirect
from django.views.generic import TemplateView
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator

@method_decorator(login_required, name='dispatch')
class UserTemplateView(TemplateView):
    template_name = "user.html"
    list_template_name = "users.html"
    custom_action = 'get' 
    restful_query_param = 'email'

    def dispatch(self, request, *args, **kwargs):
        method = 'http_method' if hasattr(request, 'http_method') else 'method'
        if getattr(request, method).upper() == 'POST' and kwargs.get(
                self.restful_query_param, False):
            setattr(request, method, 'PUT')
        request_method = getattr(request, method).lower()
        handler = self.http_method_not_allowed
        if request_method in self.http_method_names:
            handler = getattr(
                self, request_method, self.http_method_not_allowed)
            if getattr(request, method).upper() == 'GET' and not kwargs.get(
                    self.restful_query_param, False):
                self.custom_action = 'list'
                self.template_name = self.list_template_name
            if self.custom_action:
                handler = getattr(
                    self, '{}_{}'.format(request_method, self.custom_action),
                    handler if handler else self.http_method_not_allowed)
        return handler(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(UserTemplateView, self).get_context_data(**kwargs)
        data = {}
        restful_query_param = kwargs.get(self.restful_query_param, False)
        if self.custom_action == 'get' and restful_query_param:
            data['user'] = self.get_user_data()
            if data['user'] == '':
               data['tipo']= 'registra';
            else:
               data['tipo']= 'actualiza';
        elif self.custom_action == 'list':
            data['users'] = self.get_list_user_data()      
        context.update(data)
        return context
    
    def get_user_data(self):
        data = ''

        cookies = {
            'sessionid': self.request.session.session_key
        }
        
        headers = {}
        if self.request.user.oauth_access_token:
            headers = {
                'Authorization': "{} {}".format(
                    'Bearer', self.request.user.oauth_access_token)
            }
        url = '{}/user/api/v1/user/{}/'.format(
            settings.BACKEND_FOR_FRONTEND_OC, self.kwargs['email'])
        request = requests.get(url=url, cookies=cookies, headers=headers)

        if request.status_code == 200:
            data = json.loads(request.text)

        
        return data
    
    def get_list_user_data(self):
        data = {}

        cookies = {
            'sessionid': self.request.session.session_key
        }
        
        if self.request.user.oauth_access_token:
            headers = {
                'Authorization': "{} {}".format(
                    'Bearer', self.request.user.oauth_access_token)
            }
        
        url = '{}/user/api/v1/user/'.format(settings.BACKEND_FOR_FRONTEND_OC)
        request = requests.get(url=url, cookies=cookies, headers=headers, 
                               params=self.request.GET)

        if request.status_code == 200:
            request_data = json.loads(request.text)

        data = {
            'data': request_data['data'] if 'data' in request_data else [],
            'count': request_data['count'] if 'count' in request_data else 0,
            'page': request_data['page'] if 'page' in request_data else 0,
            'next': request_data['page'] + 1 if 'page' in request_data else 0,
            'pages': request_data['pages'] if 'pages' in request_data else 0,
            'limit': request_data['limit'] if 'limit' in request_data else 0,
            'prev': request_data['page'] - 1 if 'page' in request_data else 0,
        }

        return data
    
    def post(self, request, *args, **kwargs):
        changelist_url = reverse("user",kwargs={'email':request.POST['email']}) 
        json_data = json.loads(json.dumps(request.POST))

        if json_data['accion'] == '0':
            changelist_url = reverse("users") 
            
        if json_data['accion'] == '1':
            changelist_url = reverse("user",kwargs={'email':'new123@gmail.com'}) 

        files = {'file': request.FILES['photo']}

        cookies = {
            'sessionid': self.request.session.session_key
        }
        
        headers = {}
        if self.request.user.oauth_access_token:
            headers = {
                'Authorization': "{} {}".format(
                    'Bearer', self.request.user.oauth_access_token)
            }

        url = '{}/user/api/v1/user/'.format(
            settings.BACKEND_FOR_FRONTEND_OC)
        request = requests.post(url=url, cookies=cookies, headers=headers, 
                                files=files, data=json_data)
        
        if request.status_code == 200:
                data = json.loads(request.text)
        if json_data['accion'] == '2':
            data = json.loads(request.text)
            changelist_url = reverse("user",kwargs={'email':data['email']})

        return HttpResponseRedirect(changelist_url)

    def put(self, request, *args, **kwargs):
            data = ''

            changelist_url = reverse("user",kwargs={'email':request.POST['email']}) 

            json_data = json.loads(json.dumps(request.POST))

            if json_data['accion'] == '0':
                changelist_url = reverse("users") 
            
            if json_data['accion'] == '1':
                changelist_url = reverse("user",kwargs={'email':'new123@gmail.com'})

            cookies = {
                'sessionid': self.request.session.session_key
            }
            
            headers = {}
            if self.request.user.oauth_access_token:
                headers = {
                    'Authorization': "{} {}".format(
                        'Bearer', self.request.user.oauth_access_token)
                }
            
            
            url = '{}/user/api/v1/user/{}/'.format(
                settings.BACKEND_FOR_FRONTEND_OC, self.kwargs['email'])
            request = requests.put(url=url, cookies=cookies,headers=headers,
            files=files, data=json_data)

            if request.status_code == 200:
                data = json.loads(request.text)
            if json_data['accion'] == '2':
                data = json.loads(request.text)
                changelist_url = reverse("user",kwargs={'email':data['email']})
            
            return HttpResponseRedirect(changelist_url)
