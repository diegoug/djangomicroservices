import os

from .common_settings import *

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = os.environ.get('USER_MS_DJANGO_KEY', '')

SERVICE_NAME = os.environ.get('POSTGRES_USER_MS_DATABASE_NAME', '')

ALLOWED_HOSTS = []
SERVICE_ALIAS = ''

if DEBUG:
    SERVICE_ALIAS = 'dev'
    ALLOWED_HOSTS.append(
        '{}.localhost.com'.format(SERVICE_NAME).replace('_','-'))
else:
    SERVICE_ALIAS = 'prod'
    ALLOWED_HOSTS.append(
        '{}.mydomain.com'.format(SERVICE_NAME))

ALLOWED_MICROSERVICES.append('user-ms-{}'.format(SERVICE_ALIAS))

ALLOWED_HOSTS.extend(ALLOWED_MICROSERVICES)

# Application definition ------------------------------------------------------

INSTALLED_APPS.extend([
    'user',
])

MIDDLEWARE.insert(4, '{}.middleware.TimezoneMiddleware'.format(SERVICE_NAME))

ROOT_URLCONF = '{}.urls'.format(SERVICE_NAME)

# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash.
# Examples: "http://example.com/media/", "http://media.example.com/"
MEDIA_URL = '/{}/media/'.format(SERVICE_NAME)

WSGI_APPLICATION = '{}.wsgi.application'.format(SERVICE_NAME)

# Database routers ------------------------------------------------------------

DATABASE_ROUTERS = [
    '{}.django_admin_db_router.DjangoRouter'.format(SERVICE_NAME)
]

# Static files (CSS, JavaScript, Images) --------------------------------------
# https://docs.djangoproject.com/en/2.2/howto/static-files/

STATIC_URL = '/{}/static/'.format(SERVICE_NAME)

# external credentials --------------------------------------------------------
