
from django.urls import include, re_path

from .views import UserView

urlpatterns = [
    re_path(r'^api/v1/user/$',
        UserView.as_view(),
        name='data_model_list'),

    re_path(r'^api/v1/user/(?P<email>[\w.@+-]+)/$',
        UserView.as_view(),
        name='data_model_get'),

    re_path(r'^api/v1/user/(?P<id>[\w.@+-]+)/$',
        UserView.as_view(),
        name='data_model_get'),
]
