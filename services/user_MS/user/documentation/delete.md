### API to create user
- `DELETE /user/api/v1/user/<email>/`
- `DELETE /user/api/v1/user/<email>/?<help>`

#### Params
- help: (Optional) - Only the parameter without any value - Response HTML helper documentation.

#### Success Response
```json
{
    "first_name": "name2",
    "last_name": "last2"
}
```

#### Status Codes
- 201: Created.
- 403: you can not create the data model, because is invalid content type or there are no parameters.
- 403: forbidden.