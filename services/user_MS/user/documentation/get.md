### API restfull get user
- `GET /user/api/v1/user/<email>/`
- `GET /user/api/v1/user/<email>/?<help>`

#### Params
- help: (Optional) - Only the parameter without any value - Response HTML helper documentation.

#### Requests example
- `GET /user/api/v1/user/email@admin.com`

#### Success Response
```json
{
    "id": 1,
    "username": "admin",
    "email": "email@admin.com",
    "first_name": "name",
    "last_name": "last"
}
```

#### Status Codes
- 200: The request has succeeded.
- 400: Bad Requests.
