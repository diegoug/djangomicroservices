### API restfull to get users
- `GET /user/api/v1/user/`
- `GET /user/api/v1/user/?<page>&<limit>&<active>&<help>`

#### Params
- page: page > 1 - (Optional) - Positive integer - Number of the page requested.
- limit: 50 < limit > 1 - (Optional) - Positive integer - Number of the limit of requested objects.
- active: (Optional) - String (true / false) - if it is present and valid it is filtered between active and inactive
- help: (Optional) - Only the parameter without any value - Response HTML helper documentation.

#### Requests example
- `GET /user/api/v1/user/`
- `GET /user/api/v1/user/?page=2&limit=15`
- `GET /user/api/v1/user/?active=true`
- `GET /user/api/v1/user/?help`

#### Success Response
```json
{
    "data": [{...}],
    "page": 1,
    "limit": 1,
    "count": 48,
    "pages": 48,
    "message": "Data retrieved successfully."
}
```

#### Status Codes
- 200: Auctions retrieved successfully.