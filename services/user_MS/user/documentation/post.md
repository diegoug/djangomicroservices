### API to create user
- `POST /user/api/v1/user/`
- `POST /user/api/v1/user/?<help>`

#### Params
- help: (Optional) - Only the parameter without any value - Response HTML helper documentation.

#### Params JSON
- username: (Mandatory) 
- email: (Mandatory)
- first_name: (Optional)
- last_name: (Optional)
- document_number: (Optional)
- type_document: (Optional)
- phone: (Optional)
- specialty: (Optional)
- charge: (Optional)
- city: (Optional)
- country: (Optional)
- company_area: (Optional)
- company: (Optional)
- address: (Optional)
- salary: (Optional)
- is_active: (Optional)

#### Success Response
```json
{
    "username": "admin",
    "email": "email@admin.com",
    "first_name": "name",
    "last_name": "last"
}
```

#### Status Codes
- 201: Created.
- 403: you can not create the data model, because is invalid content type or there are no parameters.
- 403: forbidden.

