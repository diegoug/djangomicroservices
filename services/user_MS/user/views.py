import json
import markdown2

from math import ceil
from decimal import Decimal
from oauth2_provider.views.generic import ProtectedResourceView

from django.views.decorators.csrf import csrf_exempt
from django.views.generic import View
from django.utils.decorators import method_decorator
from django.http import HttpResponse, HttpResponseForbidden
from django.core.exceptions import ValidationError
from django.contrib.auth.decorators import login_required, permission_required
from django.db.models.fields.related import ManyToManyField

from profiles.models import User
from common.helpers.format import dict_format
from common.helpers.classes import ExtendedDispatch


@method_decorator(csrf_exempt, name='dispatch')
class UserView(ProtectedResourceView, ExtendedDispatch, View):
    custom_action = None
    user = None
    restful_query_param = 'email'

    @permission_required('user.view_ser', raise_exception=True)
    def get_list(self, request, *args, **kwargs):

        if 'help' in request.GET:
            markdown_doc = markdown2.markdown_path(
                "./user/documentation/get_list.md")
            return HttpResponse(markdown_doc)

        valid_page, page = self.validate_get_int(
            request.GET.get('page', False))
        if not valid_page or page < 1:
            page = 1

        valid_limit, limit = self.validate_get_int(
            request.GET.get('limit', False))
        if not valid_limit:
            limit = 10
        elif limit > 50:
            limit = 50
        elif limit < 1:
            limit = 1

        query = {}

        if 'active' in request.GET:
            active = request.GET.get('active') == 'true'
            query['is_active'] = active

        users = User.objects.filter(**query)

        users = users.values(
            'id', 'username', 'email', 'first_name', 'last_name', 
            'type_document', 'document_number', 'cell_phone','charge',
            'is_active')

        count = users.count()
        pages = ceil(count / limit)
        offset = (page - 1) * limit

        users = users[offset:page*limit]
        users = [dict_format(user) for user in users]

        resp = {
            'data': users,
            'page': page,
            'limit': limit,
            'count': count,
            'pages': pages,
            'message': 'Data retrieved successfully.'
        }

        return HttpResponse(json.dumps(resp),
                            content_type="application/json", status=200)

    @permission_required('user.view_user', raise_exception=True)
    def get(self, request, *args, **kwargs):

        if 'help' in request.GET:
            markdown_doc = markdown2.markdown_path(
                "./user/documentation/get.md")
            return HttpResponse(markdown_doc)

        email = kwargs.get('email')  
        userid = -1
        try:
            userid = User.objects.filter(id=email)
        except ValueError as e:
            pass

        if userid != -1:
            user = userid
        else:
            user = User.objects.filter(email=email)
        
        
        user = dict_format(user.values(
            'uuid','username', 'photo', 'document_number', 'specialty', 
            'city', 'country', 'first_name', 'last_name', 'cell_phone', 
            'email', 'type_document', 'charge', 'address', 'password','id'
        ).first())

        userid2 = -1
        try:
            userid2 = User.objects.get(id=email)
        except ValueError as e:
            pass

        if userid2 != -1:
            user2 = userid2
        else:
            user2 = User.objects.get(email=email)
        

        for salary in user2.salary.all():
            user['salary'] = self.default(salary.value)


        return HttpResponse(json.dumps(user),
                            content_type="application/json", status=200)

    @permission_required('user.add_user', raise_exception=True)
    def post(self, request, *args, **kwargs):

        if 'help' in request.GET:
            markdown_doc = markdown2.markdown_path(
                "./user/documentation/post.md")
            return HttpResponse(markdown_doc)

        invalid_request = False
        if not request.content_type in ['application/json', 'multipart/form-data']:
            invalid_request = True
        if request.content_type == 'application/json' and not request.body:
            invalid_request = True
        if invalid_request:
            message = 'you can not create the data model, ' \
                      'because is invalid content type ' \
                      'or there are no parameters'
            return HttpResponse(message, status=403)

        
        if request.content_type == 'application/json':
            body = request.body
            json_data = json.loads(body)
        elif request.content_type == 'multipart/form-data':
            json_data = json.loads(json.dumps(request.POST))

        if 'photo' in request.FILES:
            json_data['photo'] = request.FILES['photo']

        try:
            user = User(**json_data)
            user.full_clean()
        except TypeError as e:
            message = str(e).replace('User() ', '')
            return HttpResponse(message, status=403)
        except ValidationError as e:
            return HttpResponse(str(e),
                                content_type="application/json", status=403)
        user.save()

        user = dict_format(User.objects.filter(
            id=user.id).values('id', 'username', 'email', 'first_name',
                               'last_name').first())
        return HttpResponse(json.dumps(user), status=201,
                            content_type="application/json")

    @permission_required('user.change_user', raise_exception=True)
    def put(self, request, *args, **kwargs):

        if 'help' in request.GET:
            markdown_doc = markdown2.markdown_path(
                "./user/documentation/put.md")
            return HttpResponse(markdown_doc)

        invalid_request = False
        if not request.content_type in ['application/json', 'multipart/form-data']:
            invalid_request = True
        if invalid_request:
            message = 'you can not create the data model, ' \
                      'because is invalid content type ' \
                      'or there are no parameters'
            return HttpResponse(message, status=403)

        if request.content_type == 'application/json':
            body = request.body
            json_data = json.loads(body)
        elif request.content_type == 'multipart/form-data':
            json_data = json.loads(json.dumps(request.POST))

        if 'photo' in request.FILES:
            json_data['photo'] = request.FILES['photo']
        if 'documents' in request.FILES:
            json_data['documents'] = request.FILES['documents'] 

        if 'email' not in kwargs:
            message = 'you can not update data model, ' \
                      'because uid parameter are missing.'
            return HttpResponse(message, status=403)
        email = kwargs.get('email')

        m2m_keys = []
        for attr, value in User.__dict__.items():
            if hasattr(value, 'field'):
                if isinstance(value.field, ManyToManyField):
                    m2m_keys.append(attr)

        m2m_dict = {k: json_data[k] for k in m2m_keys if k in json_data}
        for key in m2m_keys:
            if key in json_data:
                del json_data[key]

        user = User.objects.filter(email=email)
        user3 = User.objects.get(email=email)
       
        if not user.exists():
            message = 'you can not update the data model, ' \
                      'because It was not found a control area with this uuid.'
            return HttpResponse(message, status=404)

        user_dict = user.values(
            'first_name', 'last_name', 'document_number', 'type_document',
            'cell_phone', 'phone', 'specialty', 'charge', 'city', 'country',
            'address', 'is_active'
        ).first()

        user_dict.update(json_data)

        try:
            valid_user = User(**user_dict)
        except TypeError as e:
            message = str(e).replace('User() ', '')
            return HttpResponse(message, status=403)
        except ValidationError as e:
            if str(e) == "{'username': ['A user with that username already exists.']}":
                pass
            else:
                return HttpResponse(str(e),
                        content_type="application/json", status=403)

        user.update(**json_data)

        user2 = User.objects.get(email=email)
        if 'photo' in request.FILES:
            user2 = User.objects.get(email=email)
            user2.photo = json_data['photo']
            user2.save()
        else:
            user2.photo = user3.photo

        model_m2m = user.first()
        if m2m_keys:
            for key in m2m_keys:
                m2m_relation = getattr(model_m2m, key)
                m2m_relation.clear()
                if key in m2m_dict:
                   val = m2m_dict[key]
                   m2m_relation.add(val)

        user = dict_format(user.values('id', 'username', 'email', 'first_name',
                                       'last_name').first())

        return HttpResponse(json.dumps(user),
                            content_type="application/json", status=201)

    @permission_required('user.delete_user', raise_exception=True)
    def delete(self, request, *args, **kwargs):

        if 'help' in request.GET:
            markdown_doc = markdown2.markdown_path(
                "./user/documentation/delete.md")
            return HttpResponse(markdown_doc)

        email = kwargs.get('email')
        user = User.objects.filter(email=email)
        if not user.exists():
            return HttpResponse(status=404)
        user.update(**{'is_active': False})

        return HttpResponse(status=202)

    @staticmethod
    def validate_get_int(s):
        if s is False:
            return False, False
        try:
            int(s)
            return True, int(s)
        except ValueError:
            pass
        try:
            import unicodedata
            unicodedata.numeric(s)
            return True, unicodedata.numeric(s)
        except (TypeError, ValueError):
            pass
        return False, False

    def default(self, obj):
        if isinstance(obj, Decimal):
            return float(obj)
        return json.JSONEncoder.default(self, obj)

        if not user.exists():
            return HttpResponse(status=404)
